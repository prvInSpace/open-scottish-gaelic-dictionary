package cymru.prv.dictionary.gaelic;

import cymru.prv.dictionary.common.DictionaryList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TestGaelicDictionary {

    @Test
    public void testIsAbleToInitialize(){
        Assertions.assertDoesNotThrow(
                () -> new GaelicDictionary()
        );
    }

    @Test
    public void testIsAbleToInitializeWithList(){
        Assertions.assertDoesNotThrow(() -> {
            DictionaryList list = new DictionaryList();
            GaelicDictionary dictionary = new GaelicDictionary(list);
            Assertions.assertNotEquals(0, dictionary.getNumberOfWords());
            Assertions.assertNotEquals(0, list.getNumberOfAmbiguousWords());
        });
    }

    @Test
    public void ensureThatVersionNumbersAreTheSame() throws IOException {
        for(var line : Files.readAllLines(Path.of( "build.gradle"))){
            if(line.startsWith("version")){
                String version = line
                        .split("\\s+")[1]
                        .replace("'", "");
                Assertions.assertEquals(version, new GaelicDictionary().getVersion());
                return;
            }
        }
        Assertions.fail("Version number not found in build.gradle");
    }

    @Test
    public void ensureThatDictionaryCanBeCompiled(){
        var dict = new GaelicDictionary();
        var obj = dict.exportDictionaryToJson();
        Assertions.assertTrue(obj.has("words"));
        Assertions.assertEquals(dict.getLanguageCode(), obj.getString("lang"));
    }

}
