package cymru.prv.dictionary.gaelic

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * Used to test the lenition component
 * @author Preben Vangberg
 */
class TestLenition {


    @Test
    fun `test that words starting with s doesn't receive a ^`(){
        Assertions.assertEquals("sheinn",
            GaelicLenition.preformLenition("seinn")
        )
    }

}