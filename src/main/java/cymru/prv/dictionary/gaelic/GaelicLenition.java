package cymru.prv.dictionary.gaelic;

import java.util.Arrays;
import java.util.Collections;


/**
 * Helper class to handle lentition in Scottish Gaelic.
 *
 * @author Zander Urq.
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class GaelicLenition {

    public static String preformLenition(String word){
        word = word
                .replaceFirst("^p", "ph")
                .replaceFirst("^t", "th")
                .replaceFirst("^c", "ch")
                .replaceFirst("^b", "bh")
                .replaceFirst("^d", "dh")
                .replaceFirst("^g", "gh")
                .replaceFirst("^m", "mh")
                .replaceFirst("^s(?!t)", "sh")
                .replaceFirst("^f", "fh");
        if(word.matches("^(a|e|u|i|o|fh|à|è|ì|ò|ù).*"))
            word = "dh'" + word;
        return word;
    }

    public static boolean isBroad(String word){
        int lastBroad = Collections.max(Arrays.asList(
                word.lastIndexOf("a"),
                word.lastIndexOf("u"),
                word.lastIndexOf("o"),
                word.lastIndexOf("à"),
                word.lastIndexOf("ù"),
                word.lastIndexOf("ò")
        ));
        int lastSlender = Collections.max(Arrays.asList(
                word.lastIndexOf("e"),
                word.lastIndexOf("i"),
                word.lastIndexOf("è"),
                word.lastIndexOf("ì")
        ));
        return lastBroad > lastSlender;
    }

}
