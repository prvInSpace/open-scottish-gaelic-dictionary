package cymru.prv.dictionary.gaelic;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;


/**
 * Represents a Scottish Gaelic dictionary.
 *
 * Creates a dictionary with the code "gd".
 *
 * @author Zander Urq.
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class GaelicDictionary extends Dictionary {

    private static final String languageCode = "gd";
    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, GaelicVerb::new,
            WordType.noun, GaelicNoun::new,
            WordType.adjective, GaelicAdjective::new,
            WordType.adposition, GaelicAdposition::new,
            WordType.conjunction, (w) -> new Word(w,WordType.conjunction)
    );

    public GaelicDictionary(){
        super(languageCode, wordTypeFromJsonMap);
    }

    public GaelicDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, languageCode, wordTypeFromJsonMap);
    }


    @Override
    public String getVersion() {
        return "1.2.0";
    }
}
