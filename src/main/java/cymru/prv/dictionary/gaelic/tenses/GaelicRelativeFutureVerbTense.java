package cymru.prv.dictionary.gaelic.tenses;

import cymru.prv.dictionary.gaelic.GaelicLenition;
import cymru.prv.dictionary.gaelic.GaelicVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public class GaelicRelativeFutureVerbTense extends GaelicVerbTense {

	public GaelicRelativeFutureVerbTense(GaelicVerb verb, JSONObject obj) {
		super(verb, obj);
	}

	@Override
	protected List<String> getDefaultSingularFirst() {
		return getDefaultOrFunction(this::getDefaultImpersonal);
	}

	@Override
	protected List<String> getDefaultSingularSecond() {
		return getDefaultOrFunction(this::getDefaultImpersonal);
	}

	@Override
	protected List<String> getDefaultSingularThird() {
		return getDefaultOrFunction(this::getDefaultImpersonal);
	}

	@Override
	protected List<String> getDefaultPluralFirst() {
		return getDefaultOrFunction(this::getDefaultImpersonal);
	}

	@Override
	protected List<String> getDefaultPluralSecond() {
		return getDefaultOrFunction(this::getDefaultImpersonal);
	}

	@Override
	protected List<String> getDefaultPluralThird() {
		return getDefaultOrFunction(this::getDefaultImpersonal);
	}

	@Override
	protected List<String> getDefaultImpersonal() {
		return Collections.singletonList(GaelicLenition.preformLenition(applyBroadOrSlender("as", "eas")));
	}
}
